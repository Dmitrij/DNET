﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNET
{
    public class DataMessage : Message
    {
        public void Reset()
        {
            readIndex = 0;
        }

        public DataMessage()
        {
            isData = true;
            isWriting = true;
        }

        public override byte[] GetBodyBytes()
        {
            if (isWriting)
                return _buffer.ToArray();
            return base.GetBodyBytes();
        }


    
    public bool isWriting{ get; set; }
        private List<byte> _buffer = new List<byte>();
        private int readIndex = 0;
        public void Serialize(ref bool value)
        { 
            int intValue = value ? 1 : 0;
            Serialize(ref intValue);
            value = intValue == 1;
        }

        public float GetFloat(int index)
    {
        return 0;
    }
        public void Serialize(ref int value)
        {
            if (isWriting)
            {
                _buffer.AddRange(BitConverter.GetBytes(value));
            }
            else
            {
                value = BitConverter.ToInt32(GetBodyBytes(), readIndex);
                readIndex += 4;
            }
        }
        public void Serialize(ref string value)
        {
			if (isWriting)
            {
                var bytes=Encoding.UTF8.GetBytes(value);
                int bytesLength = bytes.Length;
                Serialize(ref bytesLength);
                _buffer.AddRange(bytes);
            }
            else
            {
                int bytesCount = 0;
                Serialize(ref bytesCount);
                value= Encoding.UTF8.GetString(GetBodyBytes(), readIndex, bytesCount);
                readIndex += bytesCount;
            }
        }
        public void Serialize(ref float value)
        {
            if (isWriting)
            {
                _buffer.AddRange(BitConverter.GetBytes(value));
            }
            else
            {
                value = BitConverter.ToSingle(GetBodyBytes(), readIndex);
                readIndex += 4;
            }
        }

        public string GetString()
        {
            string s = string.Empty;
            Serialize(ref s);
            return s;
        }
                              internal void Initialize()
        {
            isWriting = false;

			if (_buffer != null && _buffer.Count>0)
				body=new Body(0,_buffer.ToArray ());
        }
    }
}
