﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace DNET
{
    using System.Net.Sockets;
    using System.Net;
    using System.Threading;

    public abstract class AuthentificationModule
    {
        public abstract string GetPassword(string login);
        public abstract bool Register(string login, string password);
    }

    public class Host
    {
        public AuthentificationModule auth;
        ~Host()
        {
            StopListen();
        }
        private UdpClient _client;
        private int listenPort;
        private int sendPort;

        public Host(int sendPort, int listenPort)
        {
            this.listenPort = listenPort;
            this.sendPort = sendPort;
            _client = new UdpClient()
            {
                ExclusiveAddressUse = false
            };
            _client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false);
            _client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _client.Client.Bind(new IPEndPoint(IPAddress.Any, listenPort));
        }

        public void RequestRegistration(string login, string password)
        {
            var message = new DNET.Message();
            message.type = Message.MessageType.RegistrationRequest;

            message.text = login;
            clientConnection.login = login;
            clientConnection.password = password;
            SendData(message);
        }

        public void RegisterUser(string login, string password)
        {
            auth.Register(login, password);
        }

        public Host()
        {
        }

        private SortedDictionary<short, List<MessageHandler>> _handlers = new SortedDictionary<short, List<MessageHandler>>();
        public void RegisterHandler(short messageId, MessageHandler handler)
        {
            List<MessageHandler> handlers = null;
            if (_handlers.ContainsKey(messageId))
                handlers = _handlers[messageId];
            else
                handlers = new List<MessageHandler>(1);
            handlers.Add(handler);
            _handlers[messageId] = handlers;
        }

        private Thread listenThread;
        public void Listen()
        {
            if (isListening)
                return;
            isListening = true;
            if (listenThread == null)
            {
                listenThread = new Thread(ListenInternal);
                listenThread.Start();
            }
        }
        public void StopListen()
        {
            if (!isListening)
                return;
            isListening = false;
            _client = null;
            if (listenThread != null)
            {
                listenThread = null;
            }
        }
        public delegate void MessageHandler(Message message, IPEndPoint endPoint);
        public MessageHandler onDataReceived;
        public MessageHandler onAuthSuccess;
        public MessageHandler onAuthFailure;
        private IPEndPoint _serverEndpoint;
        private ClientConnection clientConnection = new ClientConnection();
        public bool isListening { get; private set; }

        public Message Login(string login, string password)
        {
            Message m = new Message()
            {
                type = Message.MessageType.AuthenticationRequest,
                text = login
            };
            _password = password;

            SendData(m);

            return m;
        }


        public void Connect(IPAddress remote)
        {
            if (remote != null)
                _serverEndpoint = new IPEndPoint(remote, sendPort);
            Listen();
        }

        public static int SignMessage(Message message, int e, int n)
        {


            string digestString = Convert.ToBase64String(message.GetBodyBytes());
            string hashString = Hash(digestString);

            int hashInt = hashString.GetHashCode();

            int signature = FastPow(hashInt, e, n);
            string signatureString = Convert.ToBase64String(BitConverter.GetBytes(signature));

            message.text += signatureString;

            return signature;
        }

        public static bool CheckSignature(Message message, int d, int n)
        {
            int charsForSignature = Convert.ToBase64String(new byte[4]).Length;

            var bytes = new byte[16];

            string messageSignatureString = message.text.Substring(message.text.Length - charsForSignature);
            int messageSignature = BitConverter.ToInt32(Convert.FromBase64String(messageSignatureString), 0);

            messageSignature = FastPow(messageSignature, d, n);

            string digestString = Convert.ToBase64String(message.GetBodyBytes(), 0, message.GetBodyBytes().Length - charsForSignature);
            string hashString = Hash(digestString);

            int hashInt = hashString.GetHashCode() % n;
            message.text = message.text.Substring(0, message.text.Length - charsForSignature);
            return messageSignature == hashInt;
        }

        private Message SendSafeData(Message message, RC4 rc, IPEndPoint ep = null)
        {
            LogFormat("Ciphering, RC4 state {0} {1}: ", rc.i, rc.j);

            byte[] bodyBytes = message.GetBodyBytes();
            for (int i = 0; i < bodyBytes.Length; i++)
                bodyBytes[i] = (byte)(bodyBytes[i] ^ rc.GetByte());
            message.encrypted = true;
            message.SetBodyBytes(bodyBytes);


            return SendData(
                    message, ep
                );
        }
        public Message SendSafeData(Message message, IPEndPoint ep = null)
        {
            ClientConnection connection = clientConnection;
            if (_serverEndpoint == null)
            {
                connection = GetConnection(ep);

                SignMessage(message, connection.rsaE, connection.rsaN2);

                LogFormat("Signed {0} with {1} {2}", message.text, connection.rsaE, connection.rsaN2);
            }
            else
            {
                SignMessage(message, connection.rsaE, connection.rsaN);
                LogFormat("Signed {0} with {1} {2}", message.text, connection.rsaE, connection.rsaN);
            }
            message.secretEncrypted = true;
            return SendSafeData(message, connection.rc, ep);
        }
        private void SendBytes(byte[] bytes, IPEndPoint ep = null)
        {
            if (_client != null)
                _client.Send(bytes, bytes.Length, ep);
        }
        public MessageHandler onSendMessage;
        private const long defaultRCKey = 0x47e93a94;
        private static RC4 ProvideRC()
        {
            RC4 rc = new RC4(BitConverter.GetBytes(defaultRCKey));
            return rc;
        }

        public void Flush()
        {
            _outwardQueue.Clear();
        }

        public Message SendData(Message message, IPEndPoint ep = null)
        {
            if (ep == null)
                ep = _serverEndpoint;
            LogFormat("Sending to {0}: {1}:{2}", ep == null ? "null ep" : ep.ToString(), message.type, message.text);
            if (onSendMessage != null)
                onSendMessage(message, ep);

            if (_client == null)
                _outwardQueue.Enqueue(message);

            var bytes = message.ToBytes();
            SendBytes(bytes, ep);



            return message;
        }
        private void ListenCallback(IAsyncResult result)
        {
            IPEndPoint ep = null;
            var bytes = _client.EndReceive(result, ref ep);
            ProcessResponse(bytes, ep);
        }

        public int statQueueLength { get { return _client.Available; } }

        public IEnumerable<Message> outwardPipe { get { return _outwardQueue; } }

        private Queue<Message> _outwardQueue = new Queue<Message>();


        public Message ProcessResponse(byte[] bytes, IPEndPoint endPoint)
        {
            Message parsedMessage = null;
            Message.Header header = Message.ParseHeader(bytes);


            if (header.encrypted)
            {
                var connection = clientConnection;

                if (_serverEndpoint == null)
                    connection = GetConnection(endPoint);
                RC4 rc = null;
                if (header.secretEncrypted)
                    rc = connection.rc;
                else
                    rc = ProvideRC();

                LogFormat("Deciphering, RC4 state {0} {1}: ", rc.i, rc.j);

                for (int i = Message.Header.length; i < bytes.Length; i++)
                    bytes[i] = (byte)(rc.GetByte() ^ bytes[i]);


            }

            Message.Body body = Message.GetBody(bytes);

            parsedMessage = Message.FromParts(header, body);

            if (header.secretEncrypted)
            {
                var sender = clientConnection;
                if (_serverEndpoint == null)
                {
                    sender = GetConnection(endPoint);
                    LogFormat("Signature calculated: {0} {1} {2}",
                        CheckSignature(parsedMessage, sender.rsaD, sender.rsaN), sender.rsaD, sender.rsaN);
                }
                else
                {
                    LogFormat("Signature calculated: {0} {1} {2}",
                        CheckSignature(parsedMessage, sender.rsaD, sender.rsaN2), sender.rsaD, sender.rsaN2);
                }
            }

            if (onDataReceived != null)
                onDataReceived(parsedMessage, endPoint);

            if (parsedMessage.isInternal)
            {
                ProcessInternalMessage(parsedMessage, endPoint);
            }
            else
            {
                short userType = parsedMessage.userType;
                if (_handlers.ContainsKey(userType))
                {
                    var handlers = _handlers[userType];
                    foreach (var handler in handlers)
                        handler(parsedMessage, endPoint);
                }
                else
                {
                    LogFormat("No handler found for message from {0}, user type {1}", endPoint == null ? "null!" : endPoint.ToString(), parsedMessage.type);
                }
            }

            return parsedMessage;
        }
        private void ListenInternal()
        {
            while (isListening)
                while (_client != null && _client.Available > 0)
                {
                    
                    //LogFormat("Available {0}", _client.Available);
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, listenPort);
                    try
                    {
                        var bytes = _client.Receive(ref endPoint);
                        ProcessResponse(bytes, endPoint);
                    }
                    catch (System.Exception ex)
                    {
                        LogFormat("An exception! Curse them!\r\n{0}\r\n\r\n", ex);
                    }
                }
        }
        private string _password = "default_password";
        public static string Hash(string input)
        {
            var sha1 = System.Security.Cryptography.SHA1.Create();
            var inputBytes = Encoding.UTF8.GetBytes(input);
            string hash = Encoding.UTF8.GetString(sha1.ComputeHash(
                inputBytes
                ));

            return hash;
        }

        private class ClientConnection
        {
            public IPEndPoint endpoint;
            public string challenge;
            internal int dhN;
            internal int dhQ;
            internal int dhX;
            internal int dhM;
            internal int dhY;
            internal int dhK;
            internal int commonSecret
            {
                get
                {
                    return _commonSecret;
                }
                set
                {
                    _commonSecret = value;
                    rc = new RC4(BitConverter.GetBytes(_commonSecret));
                }
            }

            internal int rsaE, rsaD, rsaN;
            private int _commonSecret;
            public RC4 rc;
            internal string password;
            internal bool awaitsRegistration;
            internal string login;
            internal int rsaN2;
        }
        private List<ClientConnection> _connections = new List<ClientConnection>();
        private void RemoveConnection(IPEndPoint endPoint)
        {
            _connections.RemoveAll((con) => con.endpoint == endPoint || con.endpoint.Equals(endPoint));
        }
        private void AddConnection(ClientConnection con)
        {
            _connections.Add(con);
        }
        private ClientConnection GetConnection(IPEndPoint endPoint)
        {
            return _connections.Find((con) => con.endpoint == endPoint || con.endpoint.Equals(endPoint));
        }
        private void StartDiffyHelman(ClientConnection client)
        {
            var dhStartMessage = new Message();
            client.dhN = GeneratePrime();
            client.dhQ = GeneratePrime(client.dhN);
            client.dhX = new Random().Next(client.dhN);
            client.dhM = FastPow(client.dhQ, client.dhX, client.dhN);
            dhStartMessage.type = Message.MessageType.DiffyHelmanStart;
            dhStartMessage.SetBodyBytes(
                    BitConverter.GetBytes(client.dhN).Concat(
                    BitConverter.GetBytes(client.dhQ)).Concat(
                    BitConverter.GetBytes(client.dhM)).ToArray());

            LogFormat("Helman start: {0} {1} {2} {3}",
                client.dhN,
                client.dhQ,
                client.dhX,
                client.dhM
                );

            SendData(dhStartMessage, client.endpoint);
        }


        private void ProcessInternalMessage(Message m, IPEndPoint endPoint)
        {
            switch (m.type)
            {
                case Message.MessageType.None:
                    break;
                case Message.MessageType.RegistrationRequest:
                    {
                        string login = m.text;

                        string password = null;
                        {
                            password = auth.GetPassword(login);

                            if (password != null)
                            {
                                Message noUserMessage = new Message()
                                {
                                    type = Message.MessageType.RegistrationFailure
                                };
                                SendData(noUserMessage, endPoint);
                                break;
                            }
                        }

                        var client = new ClientConnection()
                        {
                            password = password
                        };
                        client.endpoint = endPoint;
                        if (GetConnection(endPoint) == null)
                            AddConnection(client);
                        else
                            client = GetConnection(endPoint);
                        client.awaitsRegistration = true;
                        client.login = login;
                        StartDiffyHelman(client);
                    }
                    break;
                case Message.MessageType.AuthenticationRequest:
                    {
                        string login = m.text;

                        string password = null;
                        {
                            password = auth.GetPassword(login);

                            if (password == null)
                            {
                                Message noUserMessage = new Message()
                                {
                                    type = Message.MessageType.AuthenticationFailure
                                };
                                SendData(noUserMessage, endPoint);
                                break;
                            }
                        }

                        var client = new ClientConnection()
                        {
                            password = password
                        };
                        var challenge = Message.IntegerMessage(new Random().Next(
                            0xfff8, 0xffff
                            ));

                        client.endpoint = endPoint;
                        if (GetConnection(endPoint) == null)
                            AddConnection(client);
                        else
                            client = GetConnection(endPoint);
                        client.challenge = challenge.text;

                        challenge.type = Message.MessageType.AuthenticationChallenge;
                        SendData(challenge, endPoint);
                        break;
                    }
                case Message.MessageType.AuthenticationChallenge:
                    var response = new Message();
                    response.type = Message.MessageType.AuthenticationResponse;
                    response.text = Hash(_password + m.text);
                    SendData(response);
                    break;
                case Message.MessageType.AuthenticationResponse:
                    string expectedHash = Hash(GetConnection(endPoint).password + GetConnection(endPoint).challenge);
                    string receivedHash = m.text;

                    var authResult = new Message();

                    if (expectedHash == receivedHash)
                    {
                        var client = GetConnection(endPoint);
                        authResult.type = Message.MessageType.AuthenticationSuccess;
                        LogFormat("Authentification successful for {0}", endPoint == null ? "null ep" : endPoint.ToString());
                        if (onAuthSuccess != null)
                            onAuthSuccess(m, endPoint);

                        StartDiffyHelman(client);
                    }
                    else
                    {
                        RemoveConnection(endPoint);
                        LogFormat("Authentification failed for {0}", endPoint == null ? "null ep" : endPoint.ToString());
                        authResult.type = Message.MessageType.AuthenticationFailure;
                        if (onAuthFailure != null)
                            onAuthFailure(m, endPoint);
                    }

                    SendData(authResult, endPoint);
                    break;
                case Message.MessageType.AuthenticationSuccess:
                    if (onAuthSuccess != null)
                        onAuthSuccess(m, endPoint);
                    LogFormat("Authentification successful for {0}", endPoint.ToString());
                    break;
                case Message.MessageType.AuthenticationFailure:
                    if (onAuthFailure != null)
                        onAuthFailure(m, endPoint);
                    LogFormat("Authentification failed for {0}", endPoint.ToString());
                    break;
                case Message.MessageType.DiffyHelmanStart:
                    clientConnection.dhN = BitConverter.ToInt32(m.GetBodyBytes(), 0);
                    clientConnection.dhQ = BitConverter.ToInt32(m.GetBodyBytes(), 4);
                    clientConnection.dhM = BitConverter.ToInt32(m.GetBodyBytes(), 8);

                    clientConnection.dhY = (new Random()).Next(clientConnection.dhN);
                    clientConnection.dhK = FastPow(clientConnection.dhQ, clientConnection.dhY, clientConnection.dhN);
                    clientConnection.commonSecret = FastPow(clientConnection.dhM, clientConnection.dhY, clientConnection.dhN);

                    LogFormat("Client got initial data of Helman Exchange and generated its own values: {0} {1} {2} {3} {4}",
                clientConnection.dhN,
                clientConnection.dhQ,
                clientConnection.dhY,
                clientConnection.dhM,
                clientConnection.dhK
                );


                    LogFormat("Common Secret is {0}", clientConnection.commonSecret);
                    var dhResponse = Message.IntegerMessage(clientConnection.dhK);
                    dhResponse.type = Message.MessageType.DiffyHelmanResponse;
                    SendData(dhResponse);
                    break;
                case Message.MessageType.DiffyHelmanResponse:
                    {
                        var client = GetConnection(endPoint);
                        client.dhK = m.messageInt;
                        client.commonSecret = FastPow(client.dhK, client.dhX, client.dhN);

                        LogFormat("Server got client Helman Response: {0} {1} {2} {3} {4}",
client.dhN,
client.dhQ,
client.dhX,
client.dhM,
client.dhK
);


                        LogFormat("Common Secret is {0}", client.commonSecret);
                        int rsaD, rsaE, rsaN;
                        CalculateRSA(
                            Math.Abs(client.commonSecret) / 2,
                            out rsaD,
                            out rsaE,
                            out rsaN
                            );
                        client.rsaD = rsaD;
                        client.rsaN = rsaN;

                        var rsaOpenKeyMessage = new DNET.DataMessage();
                        bool isServer = true;
                        rsaOpenKeyMessage.Serialize(ref isServer);
                        rsaOpenKeyMessage.Serialize(ref rsaE);
                        rsaOpenKeyMessage.Serialize(ref rsaN);
                        rsaOpenKeyMessage.type = Message.MessageType.RsaOpenKey;

                        SendData(rsaOpenKeyMessage, client.endpoint);

                        if (client.awaitsRegistration)
                        {
                            var safePassword = new DNET.Message();
                            safePassword.type = Message.MessageType.SafePasswordTransferRequest;
                            SendData(safePassword, client.endpoint);
                        }

                        break;
                    }
                case Message.MessageType.RsaOpenKey:
                    {
                        var dataMessage = (DataMessage)m;
                        bool isServer = false;
                        dataMessage.Serialize(ref isServer);
                        if (isServer)
                        {
                            dataMessage.Serialize(ref clientConnection.rsaE);
                            dataMessage.Serialize(ref clientConnection.rsaN);
                            int rsaE;
                            CalculateRSA(clientConnection.commonSecret,
                                out clientConnection.rsaD,
                                out rsaE,
                                out clientConnection.rsaN2);
                            var responseRsa = new DataMessage();
                            responseRsa.type = Message.MessageType.RsaOpenKey;
                            isServer = false;
                            responseRsa.Serialize(ref isServer);
                            responseRsa.Serialize(ref rsaE);
                            responseRsa.Serialize(ref clientConnection.rsaN2);

                            SendData(responseRsa);
                        }
                        else
                        {
                            var client = GetConnection(endPoint);

                            dataMessage.Serialize(ref client.rsaE);
                            dataMessage.Serialize(ref client.rsaN2);
                        }
                    }
                    break;
                case Message.MessageType.SafePasswordTransferRequest:
                    {
                        var message = new Message();
                        message.type = Message.MessageType.SafePasswordTransferResponse;
                        message.text = clientConnection.password;

                        SendSafeData(message);
                    }
                    break;
                case Message.MessageType.SafePasswordTransferResponse:
                    {
                        var client = GetConnection(endPoint);

                        client.awaitsRegistration = false;
                        client.password = m.text;

                        RegisterUser(client.login, client.password);
                    }
                    break;
            }
        }

        static int[] extended_gcd(int a, int b)
        {
            if (b == 0)
                return new[] { a, 1, 0 };
            else
            {
                var par = extended_gcd(b, a % b);
                int d = par[0], y = par[2], x = par[1];
                return new[] { d, y, x - y * (a / b) };
            }
        }



        static int inverse_modulo(int number, int modulo)
        {
            return extended_gcd(number, modulo)[1];
        }


        public static void CalculateRSA(int minValue, out int rsaD, out int rsaE, out int rsaN)
        {
            minValue = (int)Math.Sqrt(minValue / 2);
            int p = minValue - 1;
            rsaD = -1;

            rsaE = -1;
            rsaN = -1;

            while (rsaD <= 1)
            {
                p = FindNextPrime(p + 1);
                int q = FindNextPrime(p / 2);

                rsaE = FindNextPrime(new Random(minValue).Next(minValue > 1000 ? minValue / 100 : 15));

                rsaD = inverse_modulo(rsaE, (p - 1) * (q - 1));

                rsaN = p * q;
            }
        }

        static int gcd(int a, int b)// нод
        {
            while (b != 0)
            {
                a %= b;

                swap(ref a, ref b);
            }
            return a;
        }

        private static void swap(ref int a, ref int b)
        {
            int t = b;
            b = a;
            a = t;
        }

        static bool MillerRabin(int n, int a)
        {
            long s = 0;
            int t = n - 1;

            while ((t & 1) == 0)
            {
                s++;
                t >>= 1;
            }

            if (gcd(a, n) != 1)
                return false;
            long x = FastPow(a, t, n);
            if (x == 1 || x == n - 1)
                return true;

            for (long j = 0; j < s - 1; j++)
            {
                x = (x * x) % n;
                if (x == 1)
                    return false;
                if (x == n - 1)
                    return true;
            }
            return false;
        }

        private static int FindNextPrime(int test)
        {
            if ((test & 1) == 0)
                test += 1;

            while (
                       !MillerRabin(test, 2) &&
                       !MillerRabin(test, 3) &&
                       !MillerRabin(test, 5) &&
                       !MillerRabin(test, 7)
                )
                test += 2;

            return test;
        }

        private static int GeneratePrime(int maxValue = 0)
        {
            if (maxValue == 0)
                maxValue = int.MaxValue;
            int test = new Random().Next(maxValue >> 4, maxValue);

            return FindNextPrime(test);
        }

        private void LogFormat(string format, params object[] args)
        {
            string logString = string.Format(format, args);
            if (onLog != null)
                onLog(logString);
        }
        public System.Action<string> onLog;
        internal static int FastPow(int @base, int exponent, int modulus)
        {
            int result = 1;
            while (exponent > 0)
            {
                if ((exponent & 1) == 1)
                    result = (int)(Math.BigMul(result, @base) % modulus);
                exponent = exponent >> 1;
                @base = (int)(Math.BigMul(@base, @base) % modulus);
            }
            return result;

        }

    }
}
