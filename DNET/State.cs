﻿using System;
using System.Collections.Generic;
using System.Reflection;

using StateSyncTargetId=System.Int32;

namespace DNET
{
	public class SyncVarAttribute: System.Attribute
	{
		
	}

	public class State
	{
		public static void ProcessStateSync (Message message)
		{
			int stateTargetId = 0;
			(message as DataMessage).Serialize (ref stateTargetId);

			if (stateTargets.ContainsKey (stateTargetId)) 
			{
				(message as DataMessage).Reset ();
				stateTargets [stateTargetId].Serialize (message);
			}
		}
		private static IDictionary<StateSyncTargetId,State> stateTargets=new SortedDictionary<int, State>();
		private System.Reflection.FieldInfo[] _fields;
		private object[] lastValues;
		private void Serialize(Message message)
		{
			DataMessage data= message as DataMessage;
			data.Reset ();
			data.Serialize (ref _targetId);

			for (int i=0;i<_fields.Length;i++)
			{
				var field = _fields [i];
				object fieldValue=field.GetValue (this);
				
				if (fieldValue is int) {
					int typedValue = (int)fieldValue;
					data.Serialize (ref typedValue);
				} else if (fieldValue is float) {
					float typedValue = (float)fieldValue;
					data.Serialize (ref typedValue);
				} else if (fieldValue is bool) {
					bool typedValue = (bool)fieldValue;
					int placeholder = typedValue ? 1 : 0;
					data.Serialize (ref placeholder);
				} else if (fieldValue is string) {
					string typedValue = (string)fieldValue;
					data.Serialize (ref typedValue);
				} 
				/*	else if (fieldValue is ISerializable) {
					ISerializable typedValue = (ISerializable)fieldValue;
					data.Serialize (ref typedValue);
					}
					*/
			}


		}
		public Message GetStateUpdate()
		{
			DataMessage data = new DataMessage ();
			//data.userType = (short)StepStepCommon.StepStepMessageType.SyncStateMessage;
			Serialize (data);

			return data;
		}
		private StateSyncTargetId _targetId;
		private static StateSyncTargetId targetIdCounter=1;
		~State()
		{
			stateTargets.Remove (_targetId);
		}
		public State ()
		{
			var attributes=Attribute.GetCustomAttributes(this.GetType(),typeof(SyncVarAttribute));
			var fields = GetType ().GetFields ();

			_fields = Array.FindAll (fields, (field) => field.GetCustomAttributes (typeof(SyncVarAttribute), true).Length > 0);
			lastValues=new object[_fields.Length];
			_targetId = targetIdCounter++;
			stateTargets [_targetId] = this;
		}
	}
}

