﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNET
{
    class RC4
    {
        private byte[] _s = new byte[256];
        public byte i=0, j = 0;
        public byte GetByte()
        {
            i = (byte)((i + 1) % 256);
            j = (byte)((j + _s[i]) % 256);
            byte temp = _s[i];
            _s[i] = _s[j];
            _s[j] = temp;

            return _s[(byte)((_s[i] + _s[j]) % 256)];
        }
        public RC4(byte[] key)
        {
            for (int i = 0; i < _s.Length; i++)
                _s[i] = (byte)i;
            byte j = 0;
            for (int i = 0; i < _s.Length; i++)
            {
                j = (byte)((j + _s[i] + key[i % key.Length]) % 256);
                byte temp = _s[i];
                _s[i] = _s[j];
                _s[j] = temp;
            }
        }
    }
}
