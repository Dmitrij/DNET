﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNET
{
    public class Message
    {
        internal enum MessageType : byte
        {
            None = 0,
            AuthenticationRequest,

            AuthenticationChallenge,
            AuthenticationResponse,
            AuthenticationSuccess,
            AuthenticationFailure,
            DiffyHelmanStart,
            DiffyHelmanResponse,

            RegistrationRequest,
            RegistrationFailure,
            SafePasswordTransferRequest,
            SafePasswordTransferResponse,
            RsaOpenKey
        }
        public byte[] GetHeaderBytes()
        {
            return header.GetBytes();
        }
        public virtual byte[] GetBodyBytes()
        {
            return body.RefreshBody();
        }
        public byte[] ToBytes()
        {
            var bytes = new List<byte>();
            bytes.AddRange(GetHeaderBytes());

            bytes.AddRange(
                GetBodyBytes()
            );

            return bytes.ToArray();
        }

        public int messageInt
        {
            get
            {
                return body.integerValue;
            }
            set
            {
                body.integerValue = value;
            }
        }

        public bool isInternal
        {
            get
            {
                if (type == MessageType.None)
                    return false;

                return true;
            }
        }

        public string text
        {
            get { return body.stringValue; }
            set { body.stringValue = value; }
        }
        internal MessageType type
        {
            get { return header.type; }
           set { header.type = value; }
        }
                           internal bool isData { get { return header.isData; } set { header.isData = value; } }
        internal bool encrypted { get { return header.encrypted; } set { header.encrypted = value; } }
        internal bool secretEncrypted { get { return header.secretEncrypted; } set { header.secretEncrypted = value; } }

        public short userType { get { return header.userMessageType; }set { header.userMessageType = value; } }

        public static Message IntegerMessage(int value)
        {
            var message = new Message();
            message.messageInt = value;
            return message;
        }
                          public Message()
        {
            type = MessageType.None;
            body = new Body(0, new byte[0]);
        }
        internal class Header
        {
            public MessageType type;
            public short userMessageType;
            public bool encrypted;
            public bool secretEncrypted;
            internal const int length = sizeof(byte) + sizeof(short) + sizeof(byte);

            internal bool isData;

            public byte[] GetBytes()
            {
                byte[] headerBytes = new byte[length];
                byte flags = 0;
                if (encrypted)
                    flags = (byte)(flags | (0x1));
                if (secretEncrypted)
                    flags = (byte)(flags | (0x2));
                if (isData)
                    flags = (byte)(flags | (0x4));
                headerBytes[0] = (byte)type;
                byte[] shortBytes = BitConverter.GetBytes(userMessageType);
                headerBytes[1] = shortBytes[0];
                headerBytes[2] = shortBytes[1];
                headerBytes[3] = flags;
                return headerBytes;
            }
        }

        internal void SetBodyBytes(byte[] bodyBytes)
        {
            body.Initialize(0, bodyBytes);
        }

        internal static Header ParseHeader(byte[] bytes)
        {
            Header header = new Header();

            header.type = (MessageType)bytes[0];
            header.userMessageType = BitConverter.ToInt16(bytes, 1);
            byte flags = bytes[3];
            header.encrypted = (flags & 0x01) != 0;
            header.secretEncrypted = (flags & 0x02) != 0;
            header.isData = (flags & 0x04) != 0;
            return header;
        }

        internal static Body GetBody(byte[] bytes)
        {
            int startIndex = Header.length;
            Body body = new Body(startIndex, bytes);
            return body;
        }

        internal class Body
        {
            internal int integerValue
            {
                get { return BitConverter.ToInt32(_bytes, _startIndex); }
                set { Initialize(0, BitConverter.GetBytes(value)); }
            }
                    internal string stringValue
            {
                get { return Encoding.UTF8.GetString(_bytes,_startIndex,_bytes.Length-_startIndex); }
                set { _bytes = Encoding.UTF8.GetBytes(value); }

            }
            internal Body(int startIndex, byte[] bytes)
            {
                Initialize(startIndex, bytes);
            }
            internal byte this[int index] { get { return _bytes[_startIndex + index]; } }
            private byte[] _bytes = new byte[0];
            private int _startIndex;

            internal void Initialize(int startIndex, byte[] bodyBytes)
            {
                _startIndex = startIndex;
                _bytes = bodyBytes;
            }

            internal byte[] RefreshBody()
            {
                if (_startIndex>0)
                {
                    var bytes = new byte[_bytes.Length - _startIndex];
                    for (int i = 0; i < bytes.Length; i++)
                        bytes[i] = _bytes[i + _startIndex];

                    Initialize(0, bytes);
                }

                return _bytes;
            }
        }
        private Header header = new Header();
        internal Body body;
        internal static Message FromParts(Header header, Body body)
        {
            Message m = null;
            if (header.isData)
                m = new DataMessage();
            else
                m = new Message();
            m.header = header;
            m.body = body;
            if (header.isData)
                (m as DataMessage).Initialize();
            return m;
        }
    }
}
