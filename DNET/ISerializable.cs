using System;
using System.Collections.Generic;
using System.Reflection;

namespace DNET
{
	public interface ISerializable
	{
		byte[] Serialize(out int bitsCount);
		void FillWithBytes (byte[] bytes, int offset);
	}

	
}

