﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNET;

namespace Client
{
    public partial class ClientForm : Form
    {
        public ClientForm()
        {
            InitializeComponent();
            host = new DNET.Host(4000, new System.Random().Next(4001,55000));
            host.onDataReceived += OnDataReceived;
            host.onLog += OnLogReceived;

            host.RegisterHandler(1, OnMessageReceived);
        }

        private void OnMessageReceived(DNET.Message message, IPEndPoint endPoint)
        {
            AppendLog(message.text);
        }

        System.Threading.Mutex loggingMutex = new System.Threading.Mutex();

        private void AppendLog(string logMessage)
        {
            logTextBox.Invoke((Action<string>)((log) =>
                {
                    lock(logTextBox)
                    logTextBox.Text += log + Environment.NewLine;
                }), logMessage);
        }

        private void OnLogReceived(string log)
        {
            AppendLog(
                log);
        }
        private void OnDataReceived(DNET.Message message, System.Net.IPEndPoint endPoint)
        {
            AppendLog(
                string.Format(
                         "{0} sent a message: {1}:\"{2}\"",
                         endPoint.ToString(),
                         message.ToString(),
                         message.text
                       ));
        }
        private DNET.Host host;


        private void connectButton_Click(object sender, EventArgs e)
        {
            host.Connect(
                System.Net.IPAddress.Parse(serverAddressBox.Text));
            host.Login(loginBox.Text,
                passwordBox.Text);
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            var m = new DNET.Message();
            m.text = textBox3.Text;
            m.userType = 1;
            host.SendSafeData(
                m
            );
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            host.Connect(
                System.Net.IPAddress.Parse(serverAddressBox.Text));

            host.RequestRegistration(loginBox.Text, passwordBox.Text);
        }
    }
}
