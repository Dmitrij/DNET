﻿using System;
using NUnit.Framework;

using System.Linq;
namespace DNETTest

{
    using DNET;



    public class UnitTest1
    {
        [Test]
        public void PowTest()
        {
            Assert.AreEqual(16, Host.FastPow(2, 4, 42));
            Assert.AreEqual(0, Host.FastPow(2, 4, 16));

            Assert.AreEqual(884, Host.FastPow(421, 51, 907));

            int n = 97;
            int q = 47;
            int x = 88;

            int m = Host.FastPow(q, x, n);

            int y = 42;

            int secret_my = Host.FastPow(m, y, n);

            int k = Host.FastPow(q, y, n);

            int secret_kx = Host.FastPow(k, x, n);

            Assert.AreEqual(secret_kx, secret_my);

        }

        [Test]
        public void RsaSigningTest()
        {
            {
                int e = 17;
            int d = 6641;
            int n = 14351;

            var message = new DNET.Message();
            message.text = "lol kek cheburek";

            int signature = Host.SignMessage(message, e, n);

            Assert.IsTrue(Host.CheckSignature(message, d, n));

            var bytes = message.GetBodyBytes();
            bytes[0] = (byte)(~bytes[0]);
            message.SetBodyBytes(bytes);

            Assert.IsFalse(Host.CheckSignature(message, d, n));
        }
            {
                int rsaN;
                int rsaE;
                int rsaD;
                Host.CalculateRSA(50, out rsaD, out rsaE, out rsaN);
                int powE16 = Host.FastPow(16, rsaE, rsaN);
                int powD16 = Host.FastPow(powE16, rsaE, rsaN);
                Assert.AreEqual(16,powD16);
            }
        }

        private void LoginTest(string login, string password, bool successExpected)
        {
            Host client = new Host();
            Host server = new Host();

            server.auth = new MockAuthModule();
            client.Connect(null);
            var clientLogin = client.Login(login, password);

            Assert.AreEqual(DNET.Message.MessageType.AuthenticationRequest, clientLogin.type);

            ProcessMessages(client, server);

            if (!successExpected && server.outwardPipe.Any((message) => message.type == Message.MessageType.AuthenticationFailure))
            {
                //Assert.Pass("The user authentification request was turned down when they tried to log in with non-existent login");
                return;
            }


            var challengeMessage = server.outwardPipe.FirstOrDefault((message) => message.type == Message.MessageType.AuthenticationChallenge);

            Assert.NotNull(
                challengeMessage
                );

            int challenge = challengeMessage.messageInt;
            string challengeText = challengeMessage.text;
            ProcessMessages(server, client);

            var responseMessage = client.outwardPipe.FirstOrDefault((message) => message.type == Message.MessageType.AuthenticationResponse);

            string expectedResponseText = Host.Hash(password + challengeText);

            string responseText = responseMessage.text;

            Assert.AreEqual(expectedResponseText, responseText);

            ProcessMessages(client, server);

            var authSuccessMessage = server.outwardPipe.FirstOrDefault((message) => message.type == Message.MessageType.AuthenticationSuccess);
            var authFailMessage = server.outwardPipe.FirstOrDefault((message) => message.type == Message.MessageType.AuthenticationFailure);
            if (successExpected)
            {
                Assert.IsNull(authFailMessage);
                Assert.IsNotNull(authSuccessMessage);
            }
            else
            {
                Assert.IsNotNull(authFailMessage);
                Assert.IsNull(authSuccessMessage);
            }
        }
        private void ProcessMessages(Host sender, Host receiver)
        {
            foreach (var message in sender.outwardPipe)
            {
                receiver.ProcessResponse(message.ToBytes(), null);
            }

            sender.Flush();

        }

        private class MockAuthModule : AuthentificationModule
        {
            public override string GetPassword(string login)
            {
                if (login == "login")
                    return "password";

                return null;
            }

            public override bool Register(string login, string password)
            {
                return false;
            }
        }

        [Test]
        public void CorrectLoginTest()
        {
            LoginTest("login", "password", true);
        }

        [Test]
        public void IncorrectLoginTest()
        {
            LoginTest("login1", "password", false);
        }

        [Test]
        public void IncorrectPasswordTest()
        {
            LoginTest("login", "password1", false);
        }

        [Test]
        public void HostUnitTest()
        {
            Host h = new Host(4000, 4001);
            Assert.IsFalse(h.isListening, "Host is listening at startup");

            h.Listen();
            Assert.IsTrue(h.isListening, "Host isn't listening after using Listen()");

            h.Listen();
            Assert.IsTrue(h.isListening, "Host isn't listening after using Listen() twice");

            h.StopListen();
            Assert.IsFalse(h.isListening, "Host is listening after using Listen() twice and then using StopListen");

            h.StopListen();
            Assert.IsFalse(h.isListening, "Host is listening after using Listen() twice and then using StopListen twice");


            //var hostPrivates = new PrivateObject(h);
        }
    }
}
