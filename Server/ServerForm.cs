﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNET;

namespace Server
{
    public partial class ServerForm : Form
    {
        private ServerModel _model;

        private class ServerAuthModule : DNET.AuthentificationModule
        {
            private ServerModel _model;

            public ServerAuthModule(ServerModel model)
            {
                _model = model;
            }

            public override string GetPassword(string login)
            {
                var passwords = (from user in _model.Users where user.username.Equals(login) select user.password);
                if (passwords.Count() > 0)
                    return passwords.First();
                return null;
            }

            public override bool Register(string login, string password)
            {
                User user = new User();
                user.username = login;
                user.password = password;

                _model.Users.Add(user);
                
                
                return _model.SaveChanges()>0;
            }
        }

        public ServerForm()
        {
            InitializeComponent();

            _model = new ServerModel();
            host = new DNET.Host(4001, 4000);
            host.onDataReceived += OnDataReceived;
            host.onLog += OnLogReceived;
            host.RegisterHandler(1, OnMessageReceived);
            host.onAuthSuccess += onClientJoined;

            foreach (var user in _model.Users)
            {
                int id = user.userId;
            }

            _model.SaveChanges();

            host.auth = new ServerAuthModule(_model);
        }

        private void OnMessageReceived(DNET.Message message, IPEndPoint endPoint)
        {
            foreach(var ep in _connectedUsers)
            {
                try
                {
                    var resent = new DNET.Message();
                    resent.text = endPoint+": "+ message.text;
                    host.SendSafeData(resent, ep);
                }
                catch
                {

                }
            }
        }

        private List<IPEndPoint> _connectedUsers = new List<IPEndPoint>();

        private void onClientJoined(DNET.Message message, IPEndPoint endPoint)
        {
            _connectedUsers.Add(endPoint);
        }

        private void AppendLog(string logMessage)
        {
            logTextBox.Invoke((Action<string>)((log) =>
            {
                lock (logTextBox)
                    logTextBox.Text += log + Environment.NewLine;
            }), logMessage);
        }

        private void OnLogReceived(string log)
        {
            AppendLog(
                log);

        }
        private void OnDataReceived(DNET.Message message, System.Net.IPEndPoint endPoint)
        {
            AppendLog(
                string.Format(
                         "{0} sent a message: {1}:\"{2}\"",
                         endPoint.ToString(),
                         message.ToString(),
                         message.text
                       ));
        }
        private DNET.Host host;
        private bool serverActive { get { return host.isListening; } }
        private void startStopButton_onClick(object sender, EventArgs e)
        {
            if (serverActive)
            {
                host.StopListen();
                startStopButton.Text = "Start";
            }
            else
            {
                host.Listen();
                startStopButton.Text = "Stop";
            }
        }

        private void ServerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            host.StopListen();
        }
    }
}