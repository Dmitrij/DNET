namespace Server
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SQLite.CodeFirst;
    public class ServerModel : DbContext
    {
        public ServerModel()
            : base("name=ServerModel")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<ServerModel>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);

        }
        
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserSession> Sessions { get; set; }
    }
    public class User
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int userId { get; set; }
        public string username { get; set; }
        public string password { get; set; }

    }
    public class UserSession
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int sessionId { get; set; }
        public virtual User user { get; set; }

    }
    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}